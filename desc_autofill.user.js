// ==UserScript==
// @name         KG Upload Form AutoFill
// @version      0.0.6
// @description  Auto fill Upload Form with data Fetched from IMDB along with Mediainfo generation
// @author       Cepheus
// @match        https://karagarga.in/upload.php*
// @match        https://*.imdb.com/title/*
// @grant        GM_xmlhttpRequest
// @run-at       document-end
// ==/UserScript==

(async function() {
    'use strict';

    function fetchImdbPage(imdbID) {
        return new Promise((resolve, reject) => {
            const imdburl = `https://www.imdb.com/title/${imdbID}`;
            GM_xmlhttpRequest({
                method: "GET",
                url: imdburl,
                headers: {
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36",
                    "Accept-Language": "en"
                },
                anonymous: true,
                onload: function(response) {
                    resolve(response.responseText);
                },
                onerror: function(error) {
                    reject(error);
                }
            });
        });
    }

    async function imdbfetcher(imdbID) {
        try {
            const [imdbpage, titles] = await Promise.all([
                fetchImdbPage(imdbID),
                fetchakas(imdbID)
            ]);

            const data = getTitle(imdbID, imdbpage);

            if (data.title && data.title.trim() !== "") {
                let idx = titles.findIndex(title => title.toLowerCase() === data.title.toLowerCase());
                if (idx !== -1) {
                    titles.splice(idx, 1);
                }
            }

            let titlestr = titles ? titles.join(" / ") : null;
            let bbcode = await genbb(data);

            if (!titlestr && data.mainaka) {
                titlestr = data.mainaka;
            }

            if (titlestr) {
                bbcode += `\n[b][URL=https://www.imdb.com/title/${imdbID}/releaseinfo?ref_=tt_dt_aka#akas]AKA(s)[/URL][/b][b]:[/b] ${titlestr}`;
            }

            bbcode += `\n[/spoiler]\n\n[Size=4][b]Screenshots:[/b][/Size]\n`;

            insertbb(bbcode);
            await hideAutofillLoader();
        } catch (error) {
            console.error("Error fetching IMDb data or titles:", error);
            await showerrbtn();
            setTimeout(() => window.location.reload(), 2000);
        }
    }


    function selectcountry(countryname) {
        const selectelem = document.querySelector('select[name="country_id"]');
        const options = selectelem.options;

        const countrymapper = {
            "United States": "USA",
            "United Kingdom": "UK",
            "Soviet Union": "USSR",
        };

        if (typeof countryname === "string" && countryname.includes(",")) {
            const values = countryname.split(",");
            if (values.length > 5) {
                selectelem.value = "117"
                return;
            }
            countryname = values[0].trim();
        }

        if (typeof countryname === "string") {
            if (countrymapper.hasOwnProperty(countryname)) {
                countryname = countrymapper[countryname];
            }
        } else {
            selectelem.value = "117"
            return;
        }

        for (let i = 0; i < options.length; i++) {
            if (options[i].text === countryname) {
                selectelem.selectedIndex = i;
                return;
            }
        }

        selectelem.value = "117"
    }

    function langupdater(totallangs) {
        if (totallangs.length === 1) {
            document.querySelector('input[name="lang"]').value = totallangs[0].language.charAt(0).toUpperCase() + totallangs[0].language.slice(1);
        } else {
            const languageIds = totallangs.map(lang => lang.language.charAt(0).toUpperCase() + lang.language.slice(1)).join(', ');

            if (languageIds !== '') {
                document.querySelector('input[name="lang"]').value = languageIds;
            }
        }
    }

    function setmaintitle(value) {
        document.querySelector('input[name="title"]').value = value;
    }

    function setdir(dirs, crts) {
        const input = document.querySelector('input[name="director"]');
        if (dirs.length === 0) {
            if (crts.length === 0) {
                input.value = "None";
            } else {
                if (crts.length > 2) {
                    input.value = "Various";
                } else {
                    input.value = crts.join(' & ') || "None";
                }
            }
        } else {
            if (dirs.length > 2) {
                input.value = "Various";
            } else {
                input.value = dirs.join(' & ');
            }
        }
    }

    function selectgenres(allgenres) {
        const maingen = document.getElementsByName("genre_main_id")[0];
        const othergen = document.getElementsByName("subgenre")[0];
        const mainopts = maingen.options;
        const otheropts = othergen.options;

        const genreArray = allgenres.trim().split(",").map(genre => genre.trim().toLowerCase());
        if (!genreArray.includes("drama")) {
            const index = genreArray.indexOf("family");
            if (index !== -1) {
                genreArray[index] = "drama";
            }
        }
        let foundMain = false;
        let foundOthers = {};

        for (const genre of genreArray) {
            for (let i = 0; i < mainopts.length; i++) {
                if (mainopts[i].text.toLowerCase() === genre) {
                    maingen.value = mainopts[i].value;
                    foundMain = true;
                    break;
                }
            }
            if (foundMain) {
                break
            }
        }

        if (!foundMain) {
            // console.log("Main genre not found");
            return
        }

        for (const genre of genreArray) {
            if (genre !== maingen.options[maingen.selectedIndex].text.toLowerCase()) {
                for (let i = 0; i < otheropts.length; i++) {
                    if (otheropts[i].text.toLowerCase() === genre) {
                        othergen.value = otheropts[i].value;
                        // console.log("Sub genre:", genre);
                        foundOthers[genre] = true;
                        break;
                    }
                }
                if (foundOthers[genre]) {
                    break
                }
            }
        }
    }


    function fetchImdbReleaseInfo(url) {
        return new Promise((resolve, reject) => {
            GM_xmlhttpRequest({
                method: "GET",
                url: url,
                headers: {
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/125.0.0.0 Safari/537.36",
                    "Accept-Language": "en"
                },
                anonymous: true,
                onload: function(response) {
                    resolve(response);
                },
                onerror: function(error) {
                    reject(error);
                }
            });
        });
    }

    async function fetchakas(id) {
        const url = `https://www.imdb.com/title/${id}/releaseinfo`;
        let titles = [];
        let originalTitle = null;

        try {
            const response = await fetchImdbReleaseInfo(url);
            const parser = new DOMParser();
            const htmlDoc = parser.parseFromString(response.responseText, "text/html");

            const crudeinfo = htmlDoc.querySelector('[id="__NEXT_DATA__"]');
            const json = crudeinfo ? JSON.parse(crudeinfo.textContent) : {};
            const details = json?.props?.pageProps ?? {};
            const contentInfo = details.contentData?.categories;
            const akasData = contentInfo?.[1]?.section?.items ?? [];

            if (akasData.length > 0) {
                let countriesAndAKAs = akasData.filter(item => item.rowTitle || (item.listContent && item.listContent[0].subText === "(Working Title)"))
                    .map(item => ({ country: item.rowTitle || "Unknown", aka: item.listContent[0].text }));

                titles = [...new Set(countriesAndAKAs.map(item => item.aka.toLowerCase()))]
                    .map(aka => countriesAndAKAs.find(item => item.aka.toLowerCase() === aka).aka)
                    .filter(aka => !aka.toLowerCase().includes("original"));
            }

            if (titles.length === 0) {
                const liElements = htmlDoc.querySelectorAll('li[id^="rel_aka_"]');
                titles = Array.from(liElements, element => {
                    const lastSpan = element.querySelector('.ipc-metadata-list-item__list-content-item:last-child');
                    return lastSpan ? lastSpan.textContent.trim().replace(/\(.*?\)/g, '').trim().replace(/original\s*title|working\s*title|working-title/gi, '').trim() : '';
                }).filter(title => title);
            }

            if (titles.length === 0) {
                const items = htmlDoc.querySelectorAll('[data-testid="list-item"]');
                items.forEach(item => {
                    const label = item.querySelector('.ipc-metadata-list-item__label')?.textContent;
                    const title = item.querySelector('.ipc-metadata-list-item__list-content-item')?.textContent;

                    if (label === "(original title)") {
                        originalTitle = title;
                    } else if (!/\b(?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?)\b/i.test(title)) {
                        titles.push(title);
                    }
                });
            }

            titles = [...new Set(titles)];

            if (originalTitle) {
                const index = titles.indexOf(originalTitle);
                if (index > -1) titles.splice(index, 1);
            }

            return titles.length > 0 ? titles : null;
        } catch (error) {
            console.error("Error fetching data:", error);
            return null;
        }
    }

    async function addMediaInfoButton() {
        const fntawsm = document.createElement('script');
        fntawsm.src = 'https://kit.fontawesome.com/754cd11083.js';
        fntawsm.setAttribute('crossorigin', 'anonymous');
        fntawsm.onload = function() {
            const minfoloader = function(callback) {
                const script = document.createElement('script');
                script.src = 'https://unpkg.com/mediainfo.js@0.2.2/dist/umd/index.min.js';
                script.onload = callback;
                document.head.appendChild(script);
            };

            minfoloader(function() {
                const css = `
                    .file-icon-button {
                        background-color: #445672;
                        border: none;
                        border-radius: 8px;
                        padding: 8px 16px;
                        font-size: 12px;
                        font-weight: bold;
                        color: #ffffff;
                        cursor: pointer;
                        box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
                        transition: transform 0.2s, box-shadow 0.2s, background-color 0.2s;
                    }

                    .file-icon-button:hover {
                        transform: translateY(-2px);
                        box-shadow: 0 6px 15px rgba(0, 0, 0, 0.4);
                    }

                    .file-icon-button:active {
                        transform: scale(0.95);
                        animation: clickAnimation 0.2s ease-in-out;
                    }

                    @keyframes clickAnimation {
                        0% {
                            transform: scale(1);
                            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
                        }
                        50% {
                            transform: scale(0.95);
                            box-shadow: 0 6px 15px rgba(0, 0, 0, 0.4);
                        }
                        100% {
                            transform: scale(1);
                            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
                        }
                    }

                    .flex-container {
                        display: flex;
                        align-items: center;
                        justify-content: space-between;
                    }
                `;

                const styler = document.createElement("style");
                styler.textContent = css;
                document.head.appendChild(styler);

                const ripspecelem = document.evaluate("//b[contains(text(), 'Rip Specs')]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

                if (ripspecelem) {
                    const cont = document.createElement("div");
                    cont.classList.add("flex-container");

                    const minfobtn = document.createElement("button");
                    minfobtn.classList.add("file-icon-button", "icon", "far", "fa-file-video");

                    cont.appendChild(ripspecelem.cloneNode(true));
                    cont.appendChild(minfobtn);

                    ripspecelem.parentNode.replaceChild(cont, ripspecelem);

                    minfobtn.addEventListener("click", function(event) {
                        event.preventDefault();

                        const input = document.createElement('input');
                        input.type = 'file';
                        input.style.display = 'none';

                        input.addEventListener('change', function(event) {
                            const file = event.target.files[0];
                            const filename = file.name;

                            MediaInfo({ format: 'text' }, function(mediainfo) {
                                const getSize = function() {
                                    return file.size;
                                };

                                const readChunk = function(chunkSize, offset) {
                                    return new Promise(function(resolve, reject) {
                                        const reader = new FileReader();
                                        reader.onload = function(event) {
                                            if (event.target.error) {
                                                reject(event.target.error);
                                            }
                                            resolve(new Uint8Array(event.target.result));
                                        };
                                        reader.readAsArrayBuffer(file.slice(offset, offset + chunkSize));
                                    });
                                };

                                mediainfo.analyzeData(getSize, readChunk)
                                    .then(function(result) {
                                        const lines = result.split('\n');
                                        if (!lines[1].startsWith('Unique ID')) {
                                            lines.splice(1, 0, 'Complete name                            : ' + filename);
                                        } else {
                                            lines.splice(2, 0, 'Complete name                            : ' + filename);
                                        }

                                        result = lines.join('\n');

                                        const minfoinput = document.getElementById('ripspecs');
                                        if (minfoinput.value.trim() === '') {
                                            minfoinput.value = result;
                                        } else {
                                            minfoinput.value += '\n\n' + result;
                                        }
                                    })
                                    .catch(function(error) {
                                        console.error('An error occurred:', error);
                                    });
                            });
                        });

                        input.click();
                    });
                }
            });
        };

        document.head.appendChild(fntawsm);
    }

    function minfoparser(text) {
        function parsebody(lines) {
            const ret = {};
            for (let line of lines) {
                let val = line.slice(line.indexOf(':') + 1).trim();
                let key = line.slice(0, line.indexOf(':')).trim();
                key = key.toLowerCase().replace(/[ /]/g, '_');
                if (key === 'width' || key === 'height') {
                    val = parseInt(val.replace(' ', ''));
                }
                ret[key] = val;
            }
            return ret;
        }

        function getSrcType(filename) {
            const lowerFilename = filename.toLowerCase();

            if (lowerFilename.includes('.vob') || lowerFilename.includes('.ifo')) {
                return 'DVD';
            } else if (lowerFilename.includes('dvdrip') && lowerFilename.includes('vhs')) {
                return 'VHS';
            } else if (lowerFilename.includes('dvdrip') && lowerFilename.includes('tvrip')) {
                return 'TV';
            } else if ((lowerFilename.includes('web') && lowerFilename.includes('bluray')) ||
                (lowerFilename.includes('web') && lowerFilename.includes('blu-ray')) ||
                lowerFilename.includes('bdrip')) {
                return 'Blu-ray';
            } else if (lowerFilename.includes('web-dl') ||
                lowerFilename.includes('webrip') ||
                lowerFilename.includes('.web.') ||
                lowerFilename.includes('webdl')) {
                return 'WEB';
            } else if (lowerFilename.includes('bluray') ||
                lowerFilename.includes('blu-ray')) {
                return 'Blu-ray';
            } else if (lowerFilename.includes('vhs') ||
                lowerFilename.includes('vhsrip')) {
                return 'VHS';
            } else if (lowerFilename.includes('hdtv') ||
                lowerFilename.includes('tvrip')) {
                return 'TV';
            } else {
                return 'Unknown';
            }
        }

        const json = {};
        const sections = text.split(/(\r?\n){2}/g);

        let isMediaInfo = false;

        sections.forEach(section => {
            const lowerSection = section.toLowerCase();
            // console.log("Current section:", section);
            if ((lowerSection.includes('complete name') && lowerSection.includes('unique id')) ||
                (lowerSection.includes('general') && lowerSection.includes('complete name') && lowerSection.includes('format'))) {
                isMediaInfo = true;
            }
        });


        if (!isMediaInfo) {
            console.error('Rip Specs does not seem to contain MediaInfo data.');
            return null;
        }
        for (let sec of sections) {
            sec = sec.trim();
            if (sec === '') {
                continue;
            }
            sec = sec.split(/\r?\n/g);
            const secName = sec.shift().toLowerCase().split(' ')[0];
            if (secName === 'general') {
                json[secName] = parsebody(sec);
            } else {
                if (!json[secName]) {
                    json[secName] = [];
                }
                json[secName].push(parsebody(sec));
            }
        }

        // console.log("MINFO JSON: ", json);

        let vidRes = [];
        if (json.video && json.video.length > 0) {
            const vidInfo = json.video[0];
            if (vidInfo.width && vidInfo.height) {
                vidRes = [parseInt(vidInfo.width), parseInt(vidInfo.height)];
            }
        }

        const srcType = json.general && json.general.complete_name ? getSrcType(json.general.complete_name) : 'Unknown';

        const audioLangs = new Set();
        const subsLangs = new Set();

        if (json.audio) {
            json.audio.forEach(audio => {
                if (audio.language) {
                    const lang = audio.language.replace(/\([^()]*\)/g, '').trim();
                    audioLangs.add(lang);
                }
            });
        }

        let aud_comm = 0;

        if (json.audio) {
            json.audio.forEach(item => {
                if (item.title && item.title.toLowerCase().includes('commentary')) {
                    aud_comm++;
                }
            });
        }


        if (json.text) {
            json.text.forEach(text => {
                if (text.language) {
                    let lang = text.language.replace(/\([^()]*\)/g, '').trim();
                    if (text.title && text.title.toLowerCase().includes('sdh')) {
                        lang += ' [SDH]';
                    }
                    subsLangs.add(lang);
                }
            });
        }

        return {
            vidRes: vidRes,
            srcType: srcType,
            comm: aud_comm,
            audioLangs: Array.from(audioLangs),
            subsLangs: Array.from(subsLangs)
        };
    }

    function selectsrc(srctype) {
        const srcSelect = document.querySelector('select[name="source"]');
        if (srcSelect) {
            const options = srcSelect.options;
            for (let i = 0; i < options.length; i++) {
                if (options[i].value.toLowerCase() === srctype.toLowerCase()) {
                    options[i].selected = true;
                    break;
                }
            }
        } else {
            console.error('Element "source" not found');
        }
    }

    function filllang(audioLangs) {
        const audioLangInput = document.querySelector('input[name="lang"]');
        if (audioLangInput) {
            // console.log("Audio Languages:", audioLangs);
            if (audioLangs.length > 0) {
                audioLangInput.value = audioLangs.join(', ');
            }
        } else {
            console.error('Element "lang" not found');
        }
    }

    function fillsubs(subsLangs) {
        const subsInput = document.querySelector('input[name="subs"]');
        if (subsInput) {
            // console.log("Subtitle Languages:", subsLangs);
            if (subsLangs.length > 0) {
                subsInput.value = subsLangs.join(', ');
            } else {
                subsInput.value = 'None';
            }
        } else {
            console.error('Element "subs" not found');
        }
    }

    function fillhd(vidRes) {
        const selectElement = document.querySelector('select[name="hdrip"]');
        if (selectElement && vidRes.length === 2) {
            const [width, height] = vidRes;
            if ((width > 1024 && width <= 1280) || (height > 576 && height <= 720)) {
                selectElement.value = '1'; // 720p
            } else if ((width > 1280 && width <= 1920) || (height > 720 && height <= 1080)) {
                selectElement.value = '2'; // 1080p
            }
        }
    }

    function fillhtml(parsedminfo) {
        if (!parsedminfo) {
            console.error('No minfo parsed data provided.');
            return;
        }

        selectsrc(parsedminfo.srcType ?? '');
        filllang(parsedminfo.audioLangs ?? []);
        fillsubs(parsedminfo.subsLangs ?? []);
        fillhd(parsedminfo.vidRes ?? []);
    }



    function updateTitle(audio_comm) {


        if (audio_comm > 0) {


            const titleInput = document.querySelector('input[name="title"]');

            if (titleInput && titleInput.value.trim() !== "") {
                const newTitle = titleInput.value + " [+Commentary]";
                setmaintitle(newTitle);
            } else {
                console.log("No title input found or empty title.");
            }
        }
    }


    function splawards(html) {
        const parser = new DOMParser();
        const dom = parser.parseFromString(html, "text/html");
        const anchelem = dom.querySelector('a[aria-label="See more awards and nominations"]');
        if (anchelem) {
            const textcont = anchelem.textContent;
            return `[[I]${textcont}[/I]]`;
        } else {
            return null;
        }
    }

    function getTitle(imdbID, html) {
        const parser = new DOMParser();
        const dom = parser.parseFromString(html, "text/html");
        const crudeinfo = dom.querySelector('[id="__NEXT_DATA__"]');
        const json = JSON.parse(crudeinfo?.textContent ?? "{}");
        const details = json?.props?.pageProps ?? {};

        const getcredits = (lookFor, v) => {
            const credits = details?.aboveTheFoldData?.principalCredits ?? [];
            const result = credits.find((e) => e?.category?.id === lookFor);

            return result
                ? result.credits.map((e) => {
                    if (v === "2") return { id: e?.name?.id, name: e?.name?.nameText?.text };
                    return e?.name?.nameText?.text;
                })
                : [];
        };
        const awardsheader = splawards(html);
        return {
            id: imdbID,
            reviews: details.aboveTheFoldData ? `/reviews/${details.aboveTheFoldData.id}` : null,
            imdb: details.aboveTheFoldData ? `https://www.imdb.com/title/${details.aboveTheFoldData.id}` : null,
            title: details.aboveTheFoldData?.titleText?.text ?? null,
            originaltitle: details.aboveTheFoldData?.originalTitleText?.text ?? null,
            mainaka: details.mainColumnData?.akas?.edges[0]?.node?.text ?? null,
            contentType: details.aboveTheFoldData?.titleType?.id ?? null,
            contentRating: details.aboveTheFoldData?.certificate?.rating ?? "N/A",
            series: details.aboveTheFoldData?.titleType?.isSeries ?? false,
            released: details.aboveTheFoldData?.productionStatus?.currentProductionStage?.id === "released",
            image: details.aboveTheFoldData?.primaryImage?.url ?? null,
            images: details.mainColumnData?.titleMainImages?.edges?.filter((e) => e.__typename === "ImageEdge").map((e) => e.node.url) ?? [],
            plot: details.aboveTheFoldData?.plot?.plotText?.plainText ?? null,
            runtime: details.aboveTheFoldData?.runtime?.displayableProperty?.value?.plainText ?? "",
            runtimeSeconds: details.aboveTheFoldData?.runtime?.seconds ?? 0,
            rating: {
                count: details.aboveTheFoldData?.ratingsSummary?.voteCount ?? 0,
                star: details.aboveTheFoldData?.ratingsSummary?.aggregateRating ?? 0,
            },
            award: {
                wins: details.mainColumnData?.wins?.total ?? 0,
                nominations: details.mainColumnData?.nominations?.total ?? 0,
            },
            genre: details.aboveTheFoldData?.genres?.genres?.map((e) => e.id) ?? [],
            releasedetails: {
                year: details.aboveTheFoldData?.releaseDate?.year ?? null,
                releaseloc: {
                    country: details.mainColumnData?.releaseDate?.country?.text ?? null,
                    cca2: details.mainColumnData?.releaseDate?.country?.id ?? null,
                },
                origincountries: details.mainColumnData?.countriesOfOrigin?.countries?.map((e) => ({
                    country: e.text,
                    cca2: e.id,
                })) ?? [],
            },
            year: details.aboveTheFoldData?.releaseDate?.year ?? details.mainColumnData?.releaseYear?.year ?? details.aboveTheFoldData?.releaseYear?.year ?? null,
            totallangs: details.mainColumnData?.spokenLanguages?.spokenLanguages?.map((e) => ({
                language: e.text,
                id: e.id,
            })) ?? [],
            awardsinfo: awardsheader,
            actors: getcredits("cast"),
            actors_details: getcredits("cast", "2"),
            creators: getcredits("creator"),
            creators_details: getcredits("creator", "2"),
            directors: getcredits("director"),
            directors_details: getcredits("director", "2"),
            writers: getcredits("writer"),
            writers_details: getcredits("writer", "2"),
            top_credits: details.aboveTheFoldData?.principalCredits?.map((e) => ({
                id: e.category?.id ?? null,
                name: e.category?.text ?? null,
                credits: e.credits?.map((e) => e.name?.nameText?.text) ?? [],
            })) ?? [],
        };
    }

    async function genbb(imdbData) {
        const {
            id,
            title,
            originaltitle,
            year,
            plot,
            runtime,
            genre,
            totallangs,
            releasedetails,
            award,
            awardsinfo,
            rating,
            creators,
            creators_details,
            directors,
            directors_details,
            actors,
            actors_details,
            writers,
            writers_details
        } = imdbData;

        const imdbRating = (rating && rating.star !== undefined && rating.star !== null && rating.count !== undefined && rating.count !== null) ? (rating.star.toFixed(1) === "0.0" ? 'N/A' : (rating.count > 0 ? `${rating.star.toFixed(1)}/10 from ${rating.count} users` : `${rating.star.toFixed(1)}/10`)) : 'N/A';

        const hyperlinkbbcode = (items) => {
            return items.map(item => {
                if (item.id) {
                    return `[url=https://www.imdb.com/name/${item.id}]${item.name}[/url]`;
                } else {
                    return item;
                }
            }).join(", ");
        };

        const directorsBBCode = directors_details.length ? hyperlinkbbcode(directors_details) : (directors.length ? hyperlinkbbcode(directors) : 'N/A');
        const creatorsBBCode = directorsBBCode === 'N/A' ? (creators_details.length ? hyperlinkbbcode(creators_details) : (creators.length ? hyperlinkbbcode(creators) : 'N/A')) : '';

        const actorsBBCode = actors_details.length ? hyperlinkbbcode(actors_details) : (actors.length ? hyperlinkbbcode(actors) : 'N/A');
        const writersBBCode = writers_details.length ? hyperlinkbbcode(writers_details) : (writers.length ? hyperlinkbbcode(writers) : 'N/A');

        const originLocationsCountry = releasedetails.origincountries.map(location => location.country).join(", ");
        const titleSection = originaltitle && originaltitle !== title ? `${originaltitle} AKA ${title}` : title;
        const wins = award.wins || '';
        const nominations = award.nominations || '';
        const winsText = wins ? `${wins} win${wins > 1 ? 's' : ''}` : '';
        const nominationsText = nominations ? `${nominations} nomination${nominations > 1 ? 's' : ''}` : '';

        let awardsText = '';
        if (wins && nominations) {
            awardsText = `${winsText} && ${nominationsText}`;
        } else if (wins || nominations) {
            awardsText = wins ? winsText : nominationsText;
        } else {
            awardsText = 'N/A';
        }

        const awardsBBCode = awardsText !== 'N/A' ? `[b][URL=https://www.imdb.com/title/${id}/awards]Awards[/URL]:[/b] ${awardsText} ${awardsinfo !== '[[I]Awards[/I]]' ? awardsinfo : ''}` : '[b]Awards:[/b] N/A';

        selectcountry(originLocationsCountry);

        if (!document.querySelector('input[name="lang"]').value) {
            langupdater(totallangs);
        }
        selectgenres(`${genre.join(", ")}`);
        setmaintitle(titleSection);
        setdir(directors, creators);

        const txt = document.getElementById('ripspecs').value;

        if (txt) {
            const parsedminfo = minfoparser(txt);
            if (parsedminfo) {
                fillhtml(parsedminfo);
                // console.log("Parsed MINFO JSON: ", parsedminfo);
                updateTitle(parsedminfo.comm);
            }
        }

        let bbcode = `
${imdbData.image ? `[center][img]${imdbData.image}[/img][/center]\n` : ''}
[center][size=4][b][u]${titleSection}[/u][/b] [b](${year})[/b][/size][/center]

[Size=4][i][u]Synopsis:[/u][/i][/Size]
${plot ? `[Size=3]${plot}[/Size]` : '[i]Plot Not Found in IMDB[/i]'}

[spoiler=[b]Details[/b]]
[b]Year:[/b] ${year}
[b]Runtime:[/b] ${runtime ? runtime : 'N/A'}
[b]Genre(s):[/b] ${genre.join(", ")}
[b]Language:[/b] ${totallangs.map(lang => lang.language).join(", ")}
[b]Country:[/b] ${originLocationsCountry}
${awardsBBCode}
[b]IMDb Rating:[/b] ${imdbRating}
${directorsBBCode ? `[b]Director(s):[/b] ${directorsBBCode}` : ''}
${actorsBBCode !== "N/A" ? '[b][URL=https://www.imdb.com/title/' + id + '/fullcredits/cast]Cast[/URL][/b][b]:[/b]' : '[b]Cast:[/b]'} ${actorsBBCode}
${writersBBCode ? `[b]Writer(s):[/b] ${writersBBCode}` : ''}
${creatorsBBCode ? `[b]Creator(s):[/b] ${creatorsBBCode}` : ''}
    `;
        return bbcode.trim();
    }


    function insertbb(bbcode) {
        const textarea = document.getElementById('bbcodetextarea');
        if (textarea) {
            textarea.value = bbcode;
        }
    }

    async function extractIMDbID() {
        const input = document.getElementsByName("link")[0].value;
        const regex = /(?:https?:\/\/)?(?:www\.)?imdb\.com\/title\/(tt\d+)(?:\/[^\/\s]*)?/i;
        const match = input.match(regex);
        if (match && match.length > 1) {
            return match[1];
        } else {
            return null;
        }
    }

    async function imdbgrabber() {
        // const htmlContent = document.documentElement.outerHTML;
        const imdbID = await extractIMDbID();
        if (imdbID) {
            // console.log("IMDb ID found:", imdbID);
            await imdbfetcher(imdbID);
        }
    }


    var autofill_btn = document.createElement('button');
    autofill_btn.setAttribute('id', 'autofillBtn');

    autofill_btn.textContent = 'AutoFill';

    var autofill_loader = document.createElement('span');
    autofill_loader.setAttribute('id', 'autofillBtnloader');
    autofill_loader.classList.add('loader');

    autofill_btn.appendChild(autofill_loader);

    const autofillBtncss = `
        #autofillBtnloader {
            position: absolute;
            border: 4px solid rgba(0, 0, 0, 0.5);
            background-color: #445672;
            border-left: 4px solid #a1c5ff;
            border-radius: 50%;
            width: 16px;
            height: 16px;
            animation: spin 1s linear infinite;
            display: none;
        }
        #autofillBtn {
            position: relative;
            font-weight: bold;
            color: #ffffff;
            background-color: #445672;
            border: none;
            border-radius: 5px;
            padding: 12px 22px;
            font-size: 14px;
            font-family: sans-serif;
            display: flex;
            align-items: center;
            justify-content: center;
            cursor: pointer;
            box-shadow: 0 4px 12px rgba(0, 0, 0, 0.2), 0 2px 8px rgba(0, 0, 0, 0.1);
            transition: .2s;
        }
        @keyframes spin {
          0% { transform: rotate(0deg); }
          100% { transform: rotate(360deg); }
        }
    `;
    var autofillBtnstyle = document.createElement('style');
    autofillBtnstyle.innerHTML = autofillBtncss;
    document.head.appendChild(autofillBtnstyle);

    async function showAutofillLoader() {
        autofill_loader.style.display = 'inline-block';
        autofill_btn.style.color = '#445672';
        autofill_btn.disabled = true;
    }

    async function hideAutofillLoader() {
        var loader = document.getElementById('autofillBtnloader');
        var button = document.getElementById('autofillBtn');
        loader.style.display = 'none';
        button.style.color = '#ffffff';
        button.disabled = false;
    }

    async function showerrbtn() {
        const loader = document.getElementById('autofillBtnloader');
        const button = document.getElementById('autofillBtn');
        loader.style.display = 'none';
        button.innerHTML = '!';
        button.style.color = 'red';
        button.style.backgroundColor = '#ffffff';

        button.style.textShadow = '0 0 10px red, 0 0 20px red, 0 0 30px red, 0 0 40px red';
        setTimeout(() => {
            button.style.color = '#ffffff';
            button.style.backgroundColor = '#445672';
            button.innerHTML = 'AutoFill';
            button.style.textShadow = 'none';
        }, 2000);
    }

    autofill_btn.addEventListener('click', async function() {
        await showAutofillLoader();
        await imdbgrabber();
    });


    async function imdb2kgbtn() {
        const check = /^https?:\/\/(m\.)?(www\.)?imdb\.com\/title\/tt\d+/;
        const referenceCheck = /\/reference/;
        const u = window.location.href;

        if (check.test(u)) {
            const movie_id = u.match(/\/tt(\d+)/)[1];
            let ulel;

            if (referenceCheck.test(u)) {
                const nameElements = document.querySelectorAll('h3[itemprop="name"]');
                nameElements.forEach(nameElement => {
                    const yearSpan = nameElement.querySelector('.titlereference-title-year');
                    if (yearSpan) {
                        ulel = yearSpan;
                    }
                });
            } else {
                const h1el = document.querySelector('h1');
                ulel = h1el.nextElementSibling;
                while (ulel && ulel.nodeName.toLowerCase() !== "ul") {
                    ulel = ulel.nextElementSibling;
                }
            }

            if (!ulel) {
                ulel = document.createElement("div");
                const h1el = document.querySelector('h1');
                h1el.parentNode.insertBefore(ulel, h1el.nextSibling);
            }

            const spaces = document.createTextNode('\u00A0\u00A0\u00A0\u00A0');
            const uploadbtn = document.createElement('button');
            uploadbtn.innerHTML = 'Upload to KG';
            uploadbtn.setAttribute('style', 'background-color: #f5c518; border: 1px solid #d6b513; color: #000; padding: 4px 8px; border-radius: 4px; font-weight: bold;');
            uploadbtn.onclick = function() {
                window.open('https://karagarga.in/upload.php?title=Bypassed&type=1&upstep=2&imdbid=' + movie_id, '_blank');
            };

            ulel.appendChild(spaces);
            ulel.appendChild(uploadbtn);
        }
    }



    if (window.origin.includes("karagarga.in")) {

        let ddimg = document.querySelector('div#ddimagetabs');
        let pelement = ddimg.parentElement;

        const btncont = document.createElement('div');
        btncont.style.display = 'flex';
        btncont.style.justifyContent = 'flex-end';
        btncont.appendChild(autofill_btn);
        let h2 = document.evaluate("//h2[contains(text(), 'Upload a Torrent')]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        let h3 = document.evaluate("//h3[contains(text(), 'Step 2 of 3')]", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        if (h2 && h3) {
            await addMediaInfoButton();
            const imdbcheck = await extractIMDbID();
            if (imdbcheck) {
                pelement.insertBefore(btncont, ddimg);
            }
        }
    } else {
        await imdb2kgbtn();
    }

})();
