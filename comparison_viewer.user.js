// ==UserScript==
// @name Comparison Viewer
// @version 1.3
// @description View Comparisons in a convenient way
// @author Cepheus
// @match https://karagarga.in/*
// @require https://cdn.jsdelivr.net/npm/img-comparison-slider@8/dist/index.js
// @run-at document-end
// @grant none
// ==/UserScript==

(function() {
    "use strict";
    function spoilerparser(htmlContent) {
        let lines;
        if (htmlContent.includes('<br>')) {
            lines = htmlContent.split('<br>').map(line => line.trim());
        } else {
            lines = htmlContent.split('\n').map(line => line.trim());
        }

        let imgUrls = [];
        let group = [];
        let withinGroup = false;

        lines.forEach((line, index) => {

            if (line === '') {
                return;
            }

            if (line.match(/^\[comparison=/i)) {

                if (withinGroup) {
                    imgUrls.push(group);
                    group = [];
                }

                withinGroup = true;

                const urls = line.match(/<a\s+(?:[^>]*?\s+)?href="([^"]*)"|<img\s+(?:[^>]*?\s+)?src="([^"]*)"/gi);
                if (urls && urls.length >= 1) {
                    const extractedUrls = urls.map(match => {
                        const url = (match.match(/href="([^"]*)"/i) || match.match(/src="([^"]*)"/i))[1];
                        return url.replace(/,/g, '').replace(/\\comparison|comparison=/gi, '').replace(/\[\/comparison\]/gi, '');
                    });

                    const urlFromLine = detectUrltxt(line);
                    if (urlFromLine) {
                        extractedUrls.unshift(urlFromLine);
                    }

                    const uniqueUrls = [];
                    const urlSet = new Set();
                    for (const url of extractedUrls) {
                        if (!urlSet.has(url)) {
                            uniqueUrls.push(url);
                            urlSet.add(url);
                        } else {
                            const index = uniqueUrls.indexOf(url);
                            if (index !== -1) {
                                uniqueUrls.splice(index, 1);
                            }
                            uniqueUrls.push(url);
                        }
                    }

                    group.push(uniqueUrls);
                }

                return;
            } else if (line.match(/^\[\/comparison\]/i) || line.endsWith("[/comparison]")) {
                if (withinGroup) {
                    const urls = line.match(/<a\s+(?:[^>]*?\s+)?href="([^"]*)"|<img\s+(?:[^>]*?\s+)?src="([^"]*)"/gi);
                    if (urls && urls.length >= 1) {
                        const cleanedUrls = urls.map(match => {
                            const url = (match.match(/href="([^"]*)"/i) || match.match(/src="([^"]*)"/i))[1];
                            return url.replace(/,/g, '').replace(/\\comparison|comparison=/gi, '').replace(/\[\/comparison\]/gi, '');
                        });
                        group.push(cleanedUrls);
                    }
                    imgUrls.push(group);
                    group = [];
                    withinGroup = false;
                }
                return;
            }

            const urls = line.match(/<a\s+(?:[^>]*?\s+)?href="([^"]*)"|<img\s+(?:[^>]*?\s+)?src="([^"]*)"/gi);
            if (!urls || urls.length < 2) {
                return;
            }


            if (withinGroup) {
                const cleanedUrls = urls.map(match => {
                    const url = (match.match(/href="([^"]*)"/i) || match.match(/src="([^"]*)"/i))[1];
                    return url.replace(/,/g, '').replace(/\\comparison|comparison=/gi, '').replace(/\[\/comparison\]/gi, '');
                });
                group.push(cleanedUrls);
            }
        });

        if (withinGroup && group.length > 0) {
            imgUrls.push(group);
        }


        imgUrls = imgUrls.map(group => {
            return group.map(line => {
                return line.map(url => url.trim());
            });
        });

        return imgUrls;
    }



    function detectUrltxt(line) {
        const textContent = line.replace(/<[^>]*>/g, '');
        const urlRegex = /(https?:\/\/[^\s]+)/g;
        const matches = textContent.match(urlRegex);
        if (matches && matches.length > 0) {
            return matches[0];
        } else {
            return null;
        }
    }

    function extractnames(content) {
        const regex = /\[comparison=([^[\]]*)\]/gi;
        const matches = content.match(regex);
        if (!matches) return [];

        const names = matches.map(match => {
            const nameArr = match.substring(match.indexOf("=") + 1, match.indexOf("]")).split(',').map(name => name.trim());
            return nameArr;
        });

        return names;
    }


    function overlaycleaner() {
        try {
            document.querySelectorAll("#overlayContainer, .overlay-container").forEach((element) => {
                element.remove();
            });
        } catch (error) { }
    }

    function renderclicker(imgurls, overlaycontainer, names) {
        const scrollablecontent = overlaycontainer.querySelector(".scrollable-content");

        const fch = overlaycontainer.firstChild;

        const invisibleDiv = document.createElement("div");
        invisibleDiv.style.display = "none";

        overlaycontainer.insertBefore(invisibleDiv, fch);

        const closeBtnWrapper = document.createElement("div");
        closeBtnWrapper.style.display = "flex";
        closeBtnWrapper.id = "myCloseBtnWrapper";
        closeBtnWrapper.style.justifyContent = "center";
        closeBtnWrapper.style.alignItems = "center";
        closeBtnWrapper.style.position = "fixed";
        closeBtnWrapper.style.top = "20px";
        closeBtnWrapper.style.left = "82%";
        closeBtnWrapper.style.transform = "translateX(-50%)";
        closeBtnWrapper.style.zIndex = "9999";
        closeBtnWrapper.style.width = "50px";
        closeBtnWrapper.style.height = "50px";
        closeBtnWrapper.style.borderRadius = "50%";
        closeBtnWrapper.style.backgroundColor = "rgba(0, 0, 0, 0.5)";

        const closeBtn = document.createElement("button");
        closeBtn.style.color = "#ffffff";
        closeBtn.style.backgroundColor = "transparent";
        closeBtn.style.border = "none";
        closeBtn.style.fontWeight = "bold";
        closeBtn.style.fontSize = "20px";
        closeBtn.innerHTML = "X";
        closeBtn.addEventListener("click", closeviewer);
        closeBtnWrapper.appendChild(closeBtn);

        overlaycontainer.insertBefore(closeBtnWrapper, fch.nextSibling);

        imgurls.forEach((urls) => {
            const imgcontainer = document.createElement("div");
            imgcontainer.classList.add("image-container");
            imgcontainer.style.position = "relative";
            imgcontainer.style.display = "inline-block";
            imgcontainer.style.margin = "10px";
            const img = document.createElement("img");
            img.src = urls[0];
            img.dataset.urls = JSON.stringify(urls);
            img.dataset.currentIndex = 0;
            img.style.maxWidth = "none";
            img.style.width = "auto";
            img.style.height = "auto";
            img.style.margin = "0";
            img.style.display = "block";
            imgcontainer.appendChild(img);
            const nameindex = 0 % names.length;
            const nameoverlay = document.createElement("div");
            nameoverlay.classList.add("name-overlay");
            nameoverlay.textContent = names[nameindex];
            nameoverlay.style.position = "absolute";
            nameoverlay.style.top = "5px";
            nameoverlay.style.left = "50%";
            nameoverlay.style.transform = "translateX(-50%)";
            nameoverlay.style.color = "#fff";
            nameoverlay.style.fontWeight = "bold";
            nameoverlay.style.fontSize = "16px";
            nameoverlay.style.textShadow = "1px 1px 2px rgba(0,0,0,0.5)";
            nameoverlay.style.fontStyle = "italic";
            imgcontainer.appendChild(nameoverlay);

            img.addEventListener("click", function() {
                const currentIndex = parseInt(this.dataset.currentIndex);
                const nextIndex = (currentIndex + 1) % urls.length;
                this.src = urls[nextIndex];
                this.dataset.currentIndex = nextIndex;
                const nextNameIndex = nextIndex % names.length;
                nameoverlay.textContent = names[nextNameIndex];
            });
            scrollablecontent.appendChild(imgcontainer);
        });

        function closeviewer(event) {
            event.preventDefault();
            overlaycontainer.style.display = "none";
            overlaycleaner();
            closeBtnWrapper.style.display = "none";
        }
    }

    function renderslider(imgsrcs, names, container) {
        let curindex = 0;

        const btnscontainer = document.createElement("div");
        btnscontainer.id = "se";
        btnscontainer.name = "slider-elements";
        btnscontainer.style.marginBottom = "10px";
        btnscontainer.style.textAlign = "center";

        const prevbtn = document.createElement("button");
        prevbtn.innerText = "Prev";
        prevbtn.addEventListener("click", prevviewer);

        const nxtbtn = document.createElement("button");
        nxtbtn.innerText = "Next";
        nxtbtn.addEventListener("click", nxtviewer);

        btnscontainer.appendChild(prevbtn);
        btnscontainer.appendChild(document.createTextNode("\u00A0"));
        btnscontainer.appendChild(nxtbtn);

        const closebtn = document.createElement("button");
        closebtn.innerText = "Close";
        closebtn.addEventListener("click", closeviewer);

        const space = document.createElement("span");
        space.innerHTML = "&nbsp;";

        btnscontainer.appendChild(space);
        btnscontainer.appendChild(closebtn);

        container.querySelector(".scrollable-content").insertAdjacentElement("beforebegin", btnscontainer);

        container.style.padding = "20px";

        function updateviewer() {
            const imgarr = imgsrcs[curindex];

            container.querySelector(".scrollable-content").innerHTML = "";

            const imgcontwrapper = document.createElement("div");
            const drpdwnwrapper = document.createElement("div");

            const sellft = document.createElement("select");
            const selrht = document.createElement("select");
            const imgcompslider = document.createElement("img-comparison-slider");

            names.forEach((name) => {
                const optlft = document.createElement("option");
                optlft.value = name;
                optlft.text = name;
                sellft.appendChild(optlft);

                const optrht = document.createElement("option");
                optrht.value = name;
                optrht.text = name;
                selrht.appendChild(optrht);
            });

            sellft.options[0].selected = true;
            selrht.options[1].selected = true;

            sellft.addEventListener("change", updateslider);
            selrht.addEventListener("change", updateslider);

            drpdwnwrapper.appendChild(sellft);
            drpdwnwrapper.appendChild(selrht);

            imgcontwrapper.appendChild(drpdwnwrapper);
            imgcontwrapper.appendChild(imgcompslider);

            container.querySelector(".scrollable-content").appendChild(imgcontwrapper);

            updateslider();

            function updateslider() {
                imgcompslider.innerHTML = "";

                const lftindex = names.indexOf(sellft.value);
                const rhtindex = names.indexOf(selrht.value);

                const leftimg = document.createElement("img");
                leftimg.setAttribute("slot", "first");
                leftimg.src = imgarr[lftindex];

                const rightimg = document.createElement("img");
                rightimg.setAttribute("slot", "second");
                rightimg.src = imgarr[rhtindex];

                imgcompslider.appendChild(leftimg);
                imgcompslider.appendChild(rightimg);
            }
        }

        function prevviewer(event) {
            event.preventDefault();
            event.stopPropagation();
            curindex = (curindex - 1 + imgsrcs.length) % imgsrcs.length;
            updateviewer();
        }

        function nxtviewer(event) {
            event.preventDefault();
            event.stopPropagation();
            curindex = (curindex + 1) % imgsrcs.length;
            updateviewer();
        }

        function closeviewer(event) {
            event.preventDefault();
            container.style.display = "none";
            overlaycleaner();
        }
        updateviewer();
    }

    function createviewer(parentel, content) {
        const imgSets = spoilerparser(content).map((imgGroup, groupIndex) => {
            const imgurls = imgGroup.map((lineArray) => {
                return lineArray.filter((url) => url !== "");
            });

            const names = extractnames(content)[groupIndex];

            return { imgurls, names };
        });

        imgSets.forEach((set, index) => {
            let overlaycontainer;
            let viewerbtn = parentel.querySelector(`#viewerButton_${index}`);

            const namesString = set.names.join(", ");

            if (!viewerbtn) {
                viewerbtn = document.createElement("button");
                viewerbtn.id = `viewerButton_${index}`;
                viewerbtn.innerText = `Viewer`;

                const namesLabel = document.createElement("span");
                namesLabel.innerText = `${namesString}: `;

                const buttonContainer = document.createElement("div");
                buttonContainer.appendChild(namesLabel);
                buttonContainer.appendChild(viewerbtn);
                buttonContainer.appendChild(document.createElement("br"));
                buttonContainer.appendChild(document.createElement("br"));

                if (parentel) {
                    parentel.appendChild(buttonContainer);
                } else {
                    document.body.appendChild(buttonContainer);
                }
            }



            viewerbtn.addEventListener("click", function(event) {
                event.preventDefault();

                overlaycontainer = document.createElement("div");
                overlaycontainer.id = `overlayContainer_${index}`;
                overlaycontainer.className = "overlay-container";
                overlaycontainer.style.position = "fixed";
                overlaycontainer.style.top = "0";
                overlaycontainer.style.left = "0";
                overlaycontainer.style.width = "100vw";
                overlaycontainer.style.height = "100vh";
                overlaycontainer.style.overflow = "auto";
                overlaycontainer.style.zIndex = "9999";
                overlaycontainer.style.backdropFilter = "blur(5px)";

                const rendermodemenu = document.createElement("select");
                rendermodemenu.innerHTML = `
                <option value="clicker">Clicker</option>
                <option value="slider">Slider</option>
            `;
                rendermodemenu.addEventListener("change", function() {
                    const selectedmode = rendermodemenu.value;
                    const scrollablecontent = overlaycontainer.querySelector(".scrollable-content");

                    while (scrollablecontent.firstChild) {
                        scrollablecontent.removeChild(scrollablecontent.firstChild);
                    }

                    const sliderelems = document.querySelectorAll("#se");
                    sliderelems.forEach((element) => {
                        element.parentNode.removeChild(element);
                    });

                    if (selectedmode === "clicker") {
                        renderclicker(set.imgurls, overlaycontainer, set.names);
                    } else if (selectedmode === "slider") {
                        const closeBtnWrapper = document.querySelector("#myCloseBtnWrapper");
                        if (closeBtnWrapper) {
                            closeBtnWrapper.remove();
                        }

                        renderslider(set.imgurls, set.names, overlaycontainer);
                    }
                });

                const overlaycontent = document.createElement("div");
                overlaycontent.className = "overlay-content";
                overlaycontent.style.position = "absolute";
                overlaycontent.style.top = "50%";
                overlaycontent.style.left = "50%";
                overlaycontent.style.transform = "translate(-50%, -50%)";
                overlaycontent.style.backgroundColor = "#36454F";
                overlaycontent.style.borderRadius = "10px";
                overlaycontent.style.boxShadow = "0 4px 8px rgba(0, 0, 0, 0.1)";
                overlaycontent.style.maxWidth = "90%";
                overlaycontent.style.maxHeight = "90%";
                overlaycontent.style.overflow = "auto";

                overlaycontainer.appendChild(overlaycontent);

                overlaycontent.insertBefore(rendermodemenu, overlaycontent.firstChild);

                const scrollablecontent = document.createElement("div");
                scrollablecontent.className = "scrollable-content";
                scrollablecontent.style.padding = "5px";
                scrollablecontent.style.width = "calc(100% - 40px)";
                scrollablecontent.style.opacity = "1";

                overlaycontent.appendChild(scrollablecontent);

                if (parentel) {
                    parentel.appendChild(overlaycontainer);
                } else {
                    document.body.appendChild(overlaycontainer);
                }

                renderclicker(set.imgurls, overlaycontainer, set.names);
            });

            document.body.addEventListener("click", function(event) {
                if (
                    overlaycontainer &&
                    !overlaycontainer.contains(event.target) &&
                    event.target !== viewerbtn
                ) {
                    overlaycontainer.style.display = "none";
                }
            });
        });
    }

    function compcontentgen() {
        const spoilers = document.querySelectorAll(".spoiler");
        const compspoilers = [];

        spoilers.forEach((spoiler) => {
            const content = spoiler.querySelector(".spoiler-content");

            if (content && /(\[comparison=[^\]]+\][\s\S]+?\[\/comparison\])/i.test(content.innerHTML)) {
                const textContent = content.innerHTML
                    .replace(/<br\s*\/?>/gi, "\n")
                    .replace(/&nbsp;/gi, " ");

                compspoilers.push({
                    content: textContent.trim(),
                    element: spoiler,
                });
            }
        });

        if (compspoilers.length === 0) {
            // console.log("No comparison spoilers found.");
        }

        return compspoilers;
    }

    overlaycleaner();


    const style = document.createElement("link");
    style.rel = "stylesheet";
    style.href = "https://cdn.jsdelivr.net/npm/img-comparison-slider@8/dist/styles.css";
    document.head.appendChild(style);

    const sliderstyle = document.createElement("style");
    sliderstyle.textContent = `
      img-comparison-slider.rendered {
        --divider-width: 2px;
        --default-handle-shadow: 0px 0px 5px rgba(0, 0, 0, 1);
        --divider-shadow: 0px 0px 5px rgba(0, 0, 0, 0.5);
      }
    `;
    document.head.appendChild(sliderstyle);

    function comppreviewbtn() {
        const newbtn = document.createElement("input");

        newbtn.setAttribute("type", "button");
        newbtn.setAttribute("class", "codebuttons");
        newbtn.setAttribute("name", "COMP");
        newbtn.setAttribute("value", "Comp Preview");
        newbtn.style.backgroundColor = "#445672";
        newbtn.style.color = "white";
        newbtn.style.border = "none";
        newbtn.style.padding = "10px 20px";
        newbtn.style.borderRadius = "5px";
        newbtn.style.boxShadow = "0px 8px 15px rgba(0, 0, 0, 0.2), 0px 5px 8px rgba(0, 0, 0, 0.1)";
        newbtn.style.transition = "box-shadow 0.3s ease";
        newbtn.style.fontWeight = "bold";
        newbtn.style.boxShadow = "0px 4px 6px rgba(0, 0, 0, 0.1)";
        newbtn.style.fontSize = "14px";

        newbtn.addEventListener("mouseenter", function() {
            newbtn.style.boxShadow = "0px 8px 15px rgba(0, 0, 0, 0.3)";
        });

        newbtn.addEventListener("mouseleave", function() {
            newbtn.style.boxShadow = "0px 4px 6px rgba(0, 0, 0, 0.1)";
        });
        newbtn.style.display = "block";

        const langelem = document.evaluate("//p[contains(., 'List all audio tracks and their languages.')]", document, null, XPathResult.ANY_TYPE, null).iterateNext();

        if (langelem) {
            langelem.parentNode.insertBefore(newbtn, langelem);

            const br = document.createElement("br");
            newbtn.parentNode.insertBefore(br, newbtn.nextSibling);
            newbtn.parentNode.insertBefore(br, newbtn);

            newbtn.addEventListener("click", function() {
                const compspoilers = compcontentgen();

                compspoilers.forEach((comp) => {

                    if (comp.element) {
                        createviewer(comp.element, comp.content);
                    } else {
                        // console.log("Spoiler element not found.");
                    }
                });

                newbtn.style.transform = "scale(0.95)";
                setTimeout(function() {
                    newbtn.style.transform = "scale(1)";
                }, 200);
                newbtn.style.boxShadow = "inset 0px 0px 5px rgba(0, 0, 0, 0.5)";
                setTimeout(function() {
                    newbtn.style.boxShadow = "0px 4px 6px rgba(0, 0, 0, 0.1)";
                }, 200);
            });
        } else {
            // console.log("Element containing specified text not found.");
        }
    }


    function compbbcodebtn() {
        const spoilerbtn = document.querySelector("input[name='SPOILER']");
        if (spoilerbtn) {
            const compbtn = document.createElement("button");

            compbtn.innerText = "COMP";
            compbtn.className = "codebuttons";

            const fontsz = window.getComputedStyle(spoilerbtn).fontSize;
            compbtn.style.fontSize = fontsz;

            spoilerbtn.parentNode.insertBefore(compbtn, spoilerbtn.nextSibling);

            const space = document.createTextNode("\u00A0");
            spoilerbtn.parentNode.insertBefore(space, compbtn);

            compbtn.addEventListener("click", function(event) {
                event.preventDefault();
                const textarea = document.getElementById("bbcodetextarea");
                const currentval = textarea.value;
                textarea.value = currentval + "\n\n[SPOILER]\n[comparison=Source,Encode]\n\n[/comparison]\n[/SPOILER]";
            });
        } else {
            // console.log("Spoiler button not found.");
        }
    }

    function processcomps(compspoilers) {
        compspoilers.forEach((comp) => {
            if (comp.element) {
                createviewer(comp.element, comp.content);
            } else {
                // console.log("Spoiler element not found.");
            }
        });
    }

    compbbcodebtn();

    const uploadcheck = document.evaluate("//h3[contains(., 'Step 2 of 3')]", document, null, XPathResult.ANY_TYPE, null).iterateNext();

    if (uploadcheck) {
        comppreviewbtn();

    } else {

        const comps = compcontentgen();

        processcomps(comps);
    }
})();

