// ==UserScript==
// @name         KG personal uploads archiver (per page)
// @version      0.0.1
// @description  Download all Torrent files in a page as Zip archive or Torrent Links in text file
// @author       Cepheus
// @match        https://karagarga.in/mytorrents.php*
// @require      https://cdnjs.cloudflare.com/ajax/libs/jszip/3.9.1/jszip.min.js
// @grant        GM_registerMenuCommand
// @grant        GM_setValue
// @grant        GM_getValue
// @run-at       document-end
// ==/UserScript==

(function() {
    'use strict';

    setTimeout(dwnld, 1000); // Wait for 1 second after page load on the safe side
    
    function dwnld() {
        let lnks = [];
        let imgs = document.getElementsByTagName('img');

        for (let i = 0; i < imgs.length; i++) {
            let img = imgs[i];
            if (
                img.getAttribute('src') === 'pic/download.gif' &&
                img.getAttribute('border') === '0' &&
                img.getAttribute('alt') === 'Download' &&
                img.getAttribute('title') === 'Download this torrent here'
            ) {
                let lnk = fna(img);
                if (lnk) {
                    lnks.push(lnk.href);
                }
            }
        }

        if (lnks.length > 0) {
            dlbtn(lnks);
        }
    }

    function fna(elem) {
        while (elem) {
            if (elem.tagName === 'A') {
                return elem;
            }
            elem = elem.parentElement;
        }
        return null;
    }

    function txtfile(lnks) {
        let text = lnks.join('\n');
        let blob = new Blob([text], { type: 'text/plain' });
        let url = URL.createObjectURL(blob);
        let filename = `download_links-pg${curpg() + 1}.txt`;
        dwldfile(filename, url);
    }

    function gzipfile(lnks) {
        let zip = new JSZip();
        let erroredlinks = [];
    
        let prms = [];
        lnks.forEach(function(link, index) {
            let prms1 = new Promise((resolve) => {
                setTimeout(resolve, index * 150);
            }).then(() => {
                return fetch(link)
                    .then(function(response) {
                        if (response.ok) {
                            let disp = response.headers.get('content-disposition');
                            let fname = `file_${index + 1}_`;
                            if (disp && disp.indexOf('filename=') !== -1) {
                                fname += decodeURIComponent(disp.split('filename=')[1].replace(/^"(.+(?="$))"$/, '$1'));
                            }
                            return response.blob().then(function(blb) {
                                zip.file(fname, blb);
                            });
                        }
                        throw new Error(`Failed to fetch ${link}: ${response.status}`);
                    })
                    .catch(function(error) {
                        console.error(error);
                        erroredlinks.push(link);
                    });
            });
            prms.push(prms1);
        });
    
        Promise.all(prms).then(function() {
            if (erroredlinks.length > 0) {
                let erroredtxt = erroredlinks.join('\n');
                zip.file('errored_links.txt', erroredtxt);
            }
            zip.generateAsync({ type: 'blob' }).then(function(content) {
                let url = URL.createObjectURL(content);
                let filename = `download_files-pg${curpg() + 1}.zip`;
                dwldfile(filename, url);
            });
        });
    }


    function dwldfile(filename, url) {
        let a = document.createElement('a');
        a.href = url;
        a.download = filename;
        a.style.display = 'none';
        document.body.appendChild(a);
        a.click();
        URL.revokeObjectURL(url);

        let autoNext = GM_getValue('autoNext', false);
        if (autoNext) {
            let nextAnchor = nextbtn();
            if (nextAnchor) {
                nextAnchor.click();
            }
        }
    }

    function dlbtn(lnks) {
        let ddimg = document.querySelector('div#ddimagetabs');
        let pelement = ddimg.parentElement;
    
        let buttoncontainer = document.createElement('div');
        buttoncontainer.style.display = 'flex';
        buttoncontainer.style.justifyContent = 'flex-end';
    
        let btn = document.createElement('button');
        btn.innerText = 'Download All';
        btn.style.fontWeight = 'bold';
        btn.style.color = '#ffffff';
        btn.style.backgroundColor = '#445672';
        btn.style.border = 'none';
        btn.style.borderRadius = '5px';
        btn.style.padding = '8px 16px';
        btn.style.fontSize = '14px';
        btn.style.fontFamily = 'sans-serif';
        btn.style.display = 'flex';
        btn.style.alignItems = 'center';
        btn.style.justifyContent = 'center';
        btn.style.cursor = 'pointer';
        btn.style.boxShadow = '0 4px 12px rgba(0, 0, 0, 0.2), 0 2px 8px rgba(0, 0, 0, 0.1)';
    
        // Add hover effect
        btn.addEventListener('mouseenter', function() {
            btn.style.backgroundColor = '#ffffff';
            btn.style.color = '#fccf00';
        });
    
        btn.addEventListener('mouseleave', function() {
            btn.style.backgroundColor = '#445672';
            btn.style.color = '#ffffff';
        });
    
        btn.addEventListener('click', function() {
            dwnldfiles(lnks);
        });
    
        buttoncontainer.appendChild(btn);
    
        pelement.insertBefore(buttoncontainer, ddimg);
    }


    function dwnldfiles(lnks) {
        let downloadMode = GM_getValue('downloadMode', 'zip');
        if (downloadMode === 'txt') {
            txtfile(lnks);
        } else {
            gzipfile(lnks);
        }
    }

    function curpg() {
        let urlParams = new URLSearchParams(window.location.search);
        let page = urlParams.get('page');
        return page ? parseInt(page) : 0;
    }

    function settingsbox() {
        let dialoghtml = `
            <div id="settingsDialog" style="position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); background: #f7f7f7; padding: 20px; border: 2px solid #333; border-radius: 10px; box-shadow: 0 0 10px rgba(0, 0, 0, 0.3); z-index: 9999; width: 320px; height: 230px; max-width: 400px;">
                <div style="display: flex; justify-content: flex-end;">
                    <button id="closebtn" style="background: none; border: none; font-size: 20px; cursor: pointer;">&times;</button>
                </div>
                <h2 style="margin-top: 0; font-size: 24px; color: #333;">Settings</h2>
                <div id="warningmsg" style="color: red; font-size: 16px; margin-bottom: 10px; display: none;">Please Select Atleast One Download Mode</div>
                <label style="display: block; margin-bottom: 10px;">
                    <input type="checkbox" id="txtmodebox" style="margin-right: 10px;"> 
                    <span style="color: #333; font-size: 16px;">Download links as text file</span>
                </label>
                <label style="display: block; margin-bottom: 10px;">
                    <input type="checkbox" id="zipmodebox" checked style="margin-right: 10px;"> 
                    <span style="color: #333; font-size: 16px;">Download files as zip</span>
                </label>
                <label style="display: block; margin-bottom: 10px;">
                    <input type="checkbox" id="autonextbox" style="margin-right: 10px;"> 
                    <span style="color: #333; font-size: 16px;">Auto Next (Optional)</span>
                </label>
                <button id="savebtn" style="background: #007bff; color: #fff; border: none; border-radius: 5px; padding: 10px 20px; font-size: 16px; cursor: pointer;">Save</button>
            </div>
        `;

        document.body.insertAdjacentHTML('beforeend', dialoghtml);

        let closebtn = document.getElementById('closebtn');
        let txtmodebox = document.getElementById('txtmodebox');
        let zipmodebox = document.getElementById('zipmodebox');
        let autonextbox = document.getElementById('autonextbox');
        let savebtn = document.getElementById('savebtn');
        let warningmsg = document.getElementById('warningmsg');

        txtmodebox.checked = GM_getValue('downloadMode', 'zip') === 'txt';
        zipmodebox.checked = GM_getValue('downloadMode', 'zip') === 'zip';
        autonextbox.checked = GM_getValue('autoNext', false);

        txtmodebox.addEventListener('change', function() {
            if (txtmodebox.checked) {
                zipmodebox.checked = false;
            }
        });

        zipmodebox.addEventListener('change', function() {
            if (zipmodebox.checked) {
                txtmodebox.checked = false;
            }
        });

        closebtn.addEventListener('click', function() {
            document.getElementById('settingsDialog').remove();
        });

        savebtn.addEventListener('click', function() {
            if (!txtmodebox.checked && !zipmodebox.checked) {
                warningmsg.style.display = 'block';
                return;
            }

            warningmsg.style.display = 'none';
            let downloadMode = zipmodebox.checked ? 'zip' : 'txt';
            GM_setValue('downloadMode', downloadMode);
            GM_setValue('autoNext', autonextbox.checked);
            document.getElementById('settingsDialog').remove();
        });
    }

    GM_registerMenuCommand('Settings', settingsbox);

    function nextbtn() {
        let anchors = document.getElementsByTagName('a');

        for (let i = 0; i < anchors.length; i++) {
            let anchor = anchors[i];
            let boldelements = anchor.getElementsByTagName('b');
            for (let j = 0; j < boldelements.length; j++) {
                if (boldelements[j].textContent.includes('Next') && boldelements[j].textContent.includes('>')) {
                    return anchor;
                }
            }
        }

        return null;
    }
})();
