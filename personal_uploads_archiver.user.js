// ==UserScript==
// @name         KG Personal Uploads Bulk Archiver
// @version      0.0.1
// @description  Archives your uploaded Torrents (Either as Files in zip or links in text file)
// @author       Cepheus
// @match        https://karagarga.in/userdetails.php?id=*
// @grant        GM_xmlhttpRequest
// @grant        GM_registerMenuCommand
// @grant        GM_setValue
// @grant        GM_getValue
// @require      https://cdnjs.cloudflare.com/ajax/libs/jszip/3.9.1/jszip.min.js
// @run-at       document-end
// ==/UserScript==

(function() {
    'use strict';

    let myups = false;
    let dllinks = {};
    let totalpages = 0;
    let curpgnum = 0;
    let dlbtncont;
    let progresselement;
    let downloadedfiles = [];
    checker();

    if (myups) {
        createdlbtn();
    }

    function checker() {
        let myupanchor = document.querySelector('a[href="/mytorrents.php"]');
        if (myupanchor) {
            myups = true;
        } else {
            myups = false;
        }
    }


    function createdlbtn() {
        let tdrows = document.querySelectorAll('td.rowhead');
        let btnins = false;
        tdrows.forEach((td) => {
            let tbody = td.closest('tbody');
            if (tbody && !btnins) {
                let trow;
                let rows = tbody.querySelectorAll('tr');
                rows.forEach((row) => {
                    if (row.textContent.includes('Torrent Activity Details')) {
                        trow = row;
                        return;
                    }
                });
                if (trow) {
                    let dlbtncont = document.createElement('tr');
                    dlbtncont.innerHTML = `
                        <td class="rowhead" align="left">Download Your Torrents</td>
                        <td class="rowhead" style="vertical-align: middle;">
                            <button id="dlallbtn" style="font-weight: bold; color: #ffffff; background-color: #445672; border: none; border-radius: 5px; padding: 5px 14px; font-size: 11px; font-family: sans-serif; cursor: pointer; display: flex; align-items: center; justify-content: center; box-shadow: 0 4px 12px rgba(0, 0, 0, 0.2), 0 2px 8px rgba(0, 0, 0, 0.1);">Download All</button>
                        </td>
                    `;
                    tbody.insertBefore(dlbtncont, trow.nextSibling);
                    let dlallbtn = dlbtncont.querySelector('#dlallbtn');
                    dlallbtn.addEventListener('mouseenter', function() {
                        dlallbtn.style.backgroundColor = '#ffffff';
                        dlallbtn.style.color = '#fccf00';
                    });
                    dlallbtn.addEventListener('mouseleave', function() {
                         dlallbtn.style.backgroundColor = '#445672';
                         dlallbtn.style.color = '#ffffff';
                    });
                    dlallbtn.addEventListener('click', function() {
                        fetchlinks();
                    });
                    btnins = true;
                }
            }
        });
    }

    function cleanup(files) {
        files.forEach(file => {
            URL.revokeObjectURL(file);
        });
    }

    function notorrentsmsg() {
        let tdelems = document.querySelectorAll('td.rowhead');
        let tartd;
        for (let td of tdelems) {
            if (td.innerText.trim() === "Download Your Torrents") {
                tartd = td;
                break;
            }
        }
        if (tartd) {
            let msgtr = document.createElement('tr');
            let mtd = document.createElement('td');
            mtd.className = 'rowhead';
            mtd.align = 'left';
            mtd.innerText = '[No torrents found!]';
            mtd.style.color = 'red';
            msgtr.appendChild(mtd);
            tartd.closest('tr').parentNode.insertBefore(msgtr, tartd.closest('tr').nextSibling);
        } else {
            console.error('Target element not found. Unable to append the message.');
        }
    }



    function fetchlinks() {
        let myupsanch = document.querySelector('a[href="/mytorrents.php"]');
        if (myupsanch && myupsanch.innerText.trim() === "My uploads") {
            myups = true;
            fetchdllinks(myupsanch.href);
        }
    }
    function txtfile(links) {
        let alllinks = [];
        Object.keys(links).forEach(pageNumber => {
            alllinks = alllinks.concat(links[pageNumber]);
        });
        let text = alllinks.join('\n');
        let blob = new Blob([text], { type: 'text/plain' });
        let url = URL.createObjectURL(blob);
        let filename = `download_links.txt`;
        dlfile(filename, url);
    }


    function fetchdllinks(url) {
        const maxretries = 2;
        const retryint = 300;
        let retries = 0;
        let alldone = false;

        function processDownloadMode() {
            if (alldone) {
                // console.log("Final Download Links Array:", dllinks);
                let downloadMode = GM_getValue('downloadMode', 'zip');
                if (downloadMode === 'zip') {
                    createprogresselement();
                    dlfiles(dllinks);
                } else if (downloadMode === 'txt') {
                    txtfile(dllinks);
                }
            }
        }

        function fetch(url) {
            GM_xmlhttpRequest({
                method: "GET",
                url: url,
                onload: function(response) {
                    if (response.status === 200) {
                        let htmlContent = response.responseText;
                        let links = getpgdllinks(htmlContent);
                        if (Object.keys(links).length === 0 || links.length === 0) {
                            notorrentsmsg();
                            return;
                        }
                        let nxtpanc = fna(htmlContent);
                        let nxtpurl = nxtpanc ? nxtpanc.href : null;
                        let curpg = getpgnum(url);
                        dllinks[curpg] = links;
                        totalpages++;
                        if (nxtpurl) {
                            setTimeout(() => fetch(nxtpurl), 500);
                        } else {
                            alldone = true;
                            processDownloadMode();
                        }
                    } else {
                        if (retries < maxretries) {
                            retries++;
                            console.error("Failed to fetch page", url, ":", response.statusText, "- Retrying (" + retries + "/" + maxretries + ")...");
                            setTimeout(() => fetch(url), retryint);
                        } else {
                            console.error("Failed to fetch page", url, "after maximum retries.");
                            alldone = true;
                            processDownloadMode();
                        }
                    }
                }
            });
        }

        fetch(url);
    }



    function createprogresselement() {
        let dlallbtn = document.querySelector('#dlallbtn');
        if (!dlallbtn) {
            console.error("Error: Download All button element is not available.");
            return;
        }
        let parentTd = dlallbtn.closest('td');
        let progresselement = document.createElement('div');
        progresselement.id = 'progressElement';
        progresselement.style.marginLeft = '10px';
        progresselement.innerText = 'Fetching page 1 / ' + totalpages;
        try {
            parentTd.appendChild(progresselement);
        } catch (error) {
            console.error("Error appending progress element:", error);
        }
    }


    function updateprog(curpg) {
        progresselement = document.querySelector('#progressElement');
        curpgnum++;
        if (!progresselement) {
            console.error("Progress element not found.");
            return;
        }
        progresselement.innerText = 'Fetching page ' + curpgnum + ' / ' + totalpages;
    }


    function fna(htmlContent) {
        let parser = new DOMParser();
        let doc = parser.parseFromString(htmlContent, 'text/html');
        let anchors = doc.getElementsByTagName('a');
        for (let i = 0; i < anchors.length; i++) {
            let anchor = anchors[i];
            let boldElements = anchor.getElementsByTagName('b');
            for (let j = 0; j < boldElements.length; j++) {
                if (boldElements[j].textContent.includes('Next') && boldElements[j].textContent.includes('>')) {
                    return anchor;
                }
            }
        }
        return null;
    }


    function getpgdllinks(htmlContent) {
        let parser = new DOMParser();
        let doc = parser.parseFromString(htmlContent, 'text/html');
        let dllinks = [];
        let imgs = doc.getElementsByTagName('img');

        for (let i = 0; i < imgs.length; i++) {
            let img = imgs[i];
            if (
                img.getAttribute('src') === 'pic/download.gif' &&
                img.getAttribute('border') === '0' &&
                img.getAttribute('alt') === 'Download' &&
                img.getAttribute('title') === 'Download this torrent here'
            ) {
                let link = img.parentNode.href;
                if (link) {
                    dllinks.push(link);
                }
            }
        }
        return dllinks;
    }


    function getpgnum(url) {
        let match = url.match(/page=(\d+)/);
        if (match) {
            return parseInt(match[1]);
        }
        return 0;
    }


    function dlfiles(links) {
        progresselement = document.querySelector('#progressElement');
        let zip = new JSZip();
        let promises = [];
        let erroredlinks = [];
        let downloadedfiles = [];
    
        function downloadWithRetry(link, retryCount, pageNumber, index) {
            return new Promise((resolve, reject) => {
                let attempts = 0;
    
                function downloadattempt() {
                    attempts++;
                    setTimeout(() => {
                        fetch(link)
                            .then(response => {
                                if (response.ok) {
                                    let contentDisposition = response.headers.get('content-disposition');
                                    let fileName = `pg-${pageNumber}-${index + 1}`;
                                    if (contentDisposition && contentDisposition.indexOf('filename=') !== -1) {
                                        fileName += `_${decodeURIComponent(contentDisposition.split('filename=')[1].replace(/^"(.+(?="$))"$/, '$1'))}`;
                                    }
                                    return response.blob().then(blob => {
                                        zip.file(fileName, blob);
                                        downloadedfiles.push(URL.createObjectURL(blob));
                                        resolve();
                                    });
                                } else if (attempts < retryCount) {
                                    downloadattempt();
                                } else {
                                    erroredlinks.push(link);
                                    reject(new Error(`Failed to fetch ${link}: ${response.status}`));
                                }
                            })
                            .catch(error => {
                                if (attempts < retryCount) {
                                    downloadattempt();
                                } else {
                                    erroredlinks.push(link);
                                    reject(error);
                                }
                            });
                    }, (parseInt(index) + 1) * 200);
                }

                downloadattempt();
            });
        }




    
        Object.keys(links).forEach(pageNumber => {
            let pglinks = links[pageNumber];
            pglinks.forEach((link, index) => {
                let promise = downloadWithRetry(link, 2, pageNumber, index)
                    .catch(error => {
                        console.error(error);
                    });
                promises.push(promise);
            });
            updateprog(pageNumber);
        });
    
        Promise.all(promises)
            .then(() => {
                if (erroredlinks.length > 0) {
                    zip.file(`errored_links.txt`, erroredlinks.join('\n'));
                }
                return zip.generateAsync({ type: 'blob' });
            })
            .then(content => {
                dlfile('main_download_files.zip', URL.createObjectURL(content));
                cleanup(downloadedfiles);
                progresselement.remove();
            })
            .catch(error => {
                console.error('Error zipping files:', error);
            });
    }


    function dlfile(filename, url) {
        let a = document.createElement('a');
        a.href = url;
        a.download = filename;
        a.style.display = 'none';
        document.body.appendChild(a);
        a.click();
        window.URL.revokeObjectURL(url);
    }


    function settingsbox() {
        let dialogHtml = `
            <div id="settingsDialog" style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);background: #f7f7f7;padding: 26px;padding-top: 10px;border: 2px solid #333;border-radius: 10px;box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);z-index: 9999;width: 290px;height: 190px;max-width: 400px;">
               <div style="display: flex;justify-content: flex-end;">
                  <button id="closebtn" style="background: none; border: none; font-size: 27px; cursor: pointer;padding: 6px">×</button>
               </div>
               <h2 style="margin-top: 0; font-size: 24px; color: #333;">Settings</h2>
               <div id="warningmsg" style="color: red; font-size: 16px; margin-bottom: 10px; display: none;">Please Select Atleast One Download Mode</div>
               <label style="display: flex;height: 24px;margin-bottom: 10px;align-items: center;">
               <input type="radio" name="downloadMode" id="txtmodebox" style="margin: 0;margin-right: 10px;"> 
               <span style="color: #333; font-size: 16px;">Download links as text file</span>
               </label>
               <label style="display: flex;height: 10px;margin-bottom: 22px;align-items: center;">
               <input type="radio" name="downloadMode" id="zipmodebox" style="
                  margin: 0;
                  margin-right: 10px;
                  "> 
               <span style="color: #333; font-size: 16px;">Download files as zip</span>
               </label>
               <button id="savebtn" style="background: #007bff; color: #fff; border: none; border-radius: 5px; padding: 10px 20px; font-size: 16px; cursor: pointer;">Save</button>
            </div>
        `;
        document.body.insertAdjacentHTML('beforeend', dialogHtml);
        let closebtn = document.getElementById('closebtn');
        let txtmodebox = document.getElementById('txtmodebox');
        let zipmodebox = document.getElementById('zipmodebox');
        let warnmsg = document.getElementById('warningmsg');
        let savebtn = document.getElementById('savebtn');
        let curmode = GM_getValue('downloadMode', 'zip');
        if (curmode === 'txt') {
            txtmodebox.checked = true;
        } else {
            zipmodebox.checked = true;
        }
        closebtn.addEventListener('click', function() {
            document.getElementById('settingsDialog').remove();
        });
        savebtn.addEventListener('click', function() {
            if (!txtmodebox.checked && !zipmodebox.checked) {
                warnmsg.style.display = 'block';
                return;
            }
            warnmsg.style.display = 'none';
            let downloadMode = txtmodebox.checked ? 'txt' : 'zip';
            GM_setValue('downloadMode', downloadMode);
            document.getElementById('settingsDialog').remove();
        });
        txtmodebox.addEventListener('change', function() {
            if (txtmodebox.checked) {
                zipmodebox.checked = false;
            }
        });

        zipmodebox.addEventListener('change', function() {
            if (zipmodebox.checked) {
                txtmodebox.checked = false;
            }
        });
    }


    GM_registerMenuCommand('Settings', settingsbox);

})();
