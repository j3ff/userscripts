// ==UserScript==
// @name         KG Request Upgrade Approval
// @version      0.0.1
// @description  Ease the Request process for Upgrade Workflow
// @author       Cepheus
// @match        https://karagarga.in/details.php*
// @grant        GM_xmlhttpRequest
// @run-at       document-end
// ==/UserScript==

(function() {
    'use strict';

    function addMediaInfoButton() {
        const fntawsm = document.createElement('script');
        fntawsm.src = 'https://kit.fontawesome.com/754cd11083.js';
        fntawsm.setAttribute('crossorigin', 'anonymous');
        fntawsm.onload = function() {
            const minfoloader = function(callback) {
                const script = document.createElement('script');
                script.src = 'https://unpkg.com/mediainfo.js@0.2.2/dist/umd/index.min.js';
                script.onload = callback;
                document.head.appendChild(script);
            };

            minfoloader(function() {
                const css = `
                    .file-icon-button {
                        background-color: #445672;
                        border: none;
                        border-radius: 8px;
                        padding: 8px 16px;
                        font-size: 12px;
                        font-weight: bold;
                        color: #ffffff;
                        cursor: pointer;
                        box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
                        transition: transform 0.2s, box-shadow 0.2s, background-color 0.2s;
                    }

                    .file-icon-button:hover {
                        transform: translateY(-2px);
                        box-shadow: 0 6px 15px rgba(0, 0, 0, 0.4);
                    }

                    .file-icon-button:active {
                        transform: scale(0.95);
                        animation: clickAnimation 0.2s ease-in-out;
                    }

                    @keyframes clickAnimation {
                        0% {
                            transform: scale(1);
                            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
                        }
                        50% {
                            transform: scale(0.95);
                            box-shadow: 0 6px 15px rgba(0, 0, 0, 0.4);
                        }
                        100% {
                            transform: scale(1);
                            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
                        }
                    }

                    .flex-container {
                        display: flex;
                        align-items: center;
                        justify-content: space-between;
                    }
                `;

                const styler = document.createElement("style");
                styler.textContent = css;
                document.head.appendChild(styler);

                const ripspecelem = document.getElementById("overlay");

                if (ripspecelem) {


                    const overlayContent = overlay.querySelector('.overlay-content');

                    const cont = document.createElement("div");
                    cont.classList.add("flex-container");
                    cont.style.display = "flex";
                    cont.style.justifyContent = "flex-end";


                    const minfobtn = document.createElement("button");
                    minfobtn.classList.add("file-icon-button", "icon", "far", "fa-file-video");

                    cont.appendChild(minfobtn);

                    overlayContent.insertBefore(cont, overlayContent.firstChild);


                    minfobtn.addEventListener("click", function(event) {
                        event.preventDefault();

                        const input = document.createElement('input');
                        input.type = 'file';
                        input.style.display = 'none';

                        input.addEventListener('change', function(event) {
                            const file = event.target.files[0];
                            const filename = file.name;

                            MediaInfo({ format: 'text' }, function(mediainfo) {
                                const getSize = function() {
                                    return file.size;
                                };

                                const readChunk = function(chunkSize, offset) {
                                    return new Promise(function(resolve, reject) {
                                        const reader = new FileReader();
                                        reader.onload = function(event) {
                                            if (event.target.error) {
                                                reject(event.target.error);
                                            }
                                            resolve(new Uint8Array(event.target.result));
                                        };
                                        reader.readAsArrayBuffer(file.slice(offset, offset + chunkSize));
                                    });
                                };

                                mediainfo.analyzeData(getSize, readChunk)
                                    .then(function(result) {
                                        const lines = result.split('\n');
                                        if (!lines[1].startsWith('Unique ID')) {
                                            lines.splice(1, 0, 'Complete name                            : ' + filename);
                                        } else {
                                            lines.splice(2, 0, 'Complete name                            : ' + filename);
                                        }

                                        result = lines.join('\n');

                                        const minfoinput = document.querySelector("#overlay .overlay-content textarea#message");
                                        if (minfoinput.value.trim() === '') {
                                            minfoinput.value = result + '\nPlease review this release for upgrade,\nThanks!!';
                                        } else {
                                            const regex = /\[B\]Upgrade Release Name:\[\/B\]/m;
                                            minfoinput.value = minfoinput.value.replace(regex, `$& ${filename}`);
                                            minfoinput.value += result + '\nPlease review this release for upgrade,\nThanks!!';
                                        }

                                    })
                                    .catch(function(error) {
                                        console.error('An error occurred:', error);
                                    });
                            });
                        });

                        input.click();
                    });
                }
            });
        };

        document.head.appendChild(fntawsm);
    }


    function upgradeReqBox() {
        const ov = document.createElement("div");
        ov.id = "overlay";
        ov.className = "overlay";
        ov.style.display = "none";
        ov.style.cssText = `
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: #eeeeee;
    color: black;
    border-radius: 10px;
    padding: 20px;
    width: 50vw;
    height: 270px;
    max-width: 60vw;
    max-height: 60vh;
    overflow: auto;
  `;
        document.body.appendChild(ov);

        const ovContent = document.createElement("div");
        ovContent.className = "overlay-content";
        ov.appendChild(ovContent);

        const titleEl = document.querySelector("h1");
        const ttlText = titleEl.textContent.trim().split(" - ")[1].trim();
        let titleText = titleEl.textContent.trim().split(" - ")[1].trim();
        titleText = `Upgrade Approval Request for ${titleText}`;

        ovContent.innerHTML += `<label for="user">To:</label>`;
        const modMenu = document.createElement("select");
        modMenu.id = "user";
        mods.forEach(function(mod) {
            const option = document.createElement("option");
            option.value = mod;
            option.textContent = mod;
            modMenu.appendChild(option);
        });
        ovContent.appendChild(modMenu);
        ovContent.appendChild(document.createElement("br"));

        ovContent.innerHTML += `<label for="title">Title:</label>`;
        ovContent.innerHTML += `<input type="text" id="title" value="${titleText}">`;
        ovContent.appendChild(document.createElement("br"));


        const msgTemplate = `
Hello, I would like to upgrade ${ttlText}

[B]Site link:[/B] ${window.location.href}

[B]Upgrade Release Name:[/B]
[B]Additional Info:[/B]

[B]Screenshots:[/B]


[B]Mediainfo:[/B]
`;

        ovContent.innerHTML += `<label for="message">Message:</label>`;
        ovContent.innerHTML += `<textarea id="message" rows="10" style="resize: both; width: 100%;">${msgTemplate}</textarea>`;
        ovContent.appendChild(document.createElement("br"));

        const btnCont = document.createElement("div");
        btnCont.className = "button-container";
        btnCont.style.cssText = `
    display: flex;
    justify-content: space-between;
    margin-top: 20px;
  `;
        ovContent.appendChild(btnCont);

        const sendBtn = document.createElement("button");
        sendBtn.textContent = "Send";
        sendBtn.addEventListener("click", function(event) {
            event.preventDefault();
            sendMsg();
        });
        btnCont.appendChild(sendBtn);

        const clsBtn = document.createElement("button");
        clsBtn.textContent = "Close";
        clsBtn.addEventListener("click", function(event) {
            event.preventDefault();
            clsOv();
        });
        btnCont.appendChild(clsBtn);

        ov.style.display = "block";

        function sendMsg() {
            const staff = document.getElementById("user").value;
            const title = document.getElementById("title").value;
            const message = document.getElementById("message").value;
            const msgUrl = 'https://karagarga.in/messages.php';
            const usrnm = staff;
            upgradereq(msgUrl, usrnm, title, message);
            clsOv();
        }

        function clsOv() {
            ov.remove();
        }

        function upgradereq(url, usrnm, title, message) {
            fetch(url, {
                method: 'POST',
                body: new URLSearchParams({
                    'action': 'compose',
                    'send': 'y',
                    'to': usrnm,
                    'subject': title,
                    'msg': message,
                    'save': '1'
                })
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Something went wrong');
                    }
                    console.log('Message sent successfully!');
                    // alert('Message sent successfully!');
                })
                .catch(error => {
                    console.error('There was a problem sending the message:', error);
                    alert('Message was not sent, Retry or Do message manually!');
                });
        }

    }

    function isTrumpable() {
        const anchors = document.getElementsByTagName('a');
        for (let i = 0; i < anchors.length; i++) {
            if (anchors[i].getAttribute('href') === '/rules.php#upgrading' && anchors[i].textContent.trim() === 'Trumpable') {
                return true;
            }
        }
        return false;
    }


    let mods;

    function fetchmods(url, callback) {
        GM_xmlhttpRequest({
            method: "GET",
            url: url,
            onload: function(response) {
                if (response.status === 200) {
                    callback(response.responseText);
                } else {
                    console.error("Failed to fetch: " + url);
                    callback(null);
                }
            },
            onerror: function(error) {
                console.error("Error: " + url, error);
                callback(null);
            }
        });
    }

    function extractnm(html) {
        const p = new DOMParser();
        const doc = p.parseFromString(html, "text/html");
        const tbodyEl = doc.querySelectorAll('tbody');
        const tgtbody = Array.from(tbodyEl).find(function(tbody) {
            const fR = tbody.querySelector('tr:first-child');
            if (fR) {
                const tds = fR.querySelectorAll('td');
                return tds.length === 5 && tds[0].textContent.trim() === "User name";
            }
            return false;
        });
        if (!tgtbody) {
            return [];
        }
        const unmTds = tgtbody.querySelectorAll('tr td:first-child');
        const unmArr = [];
        unmTds.forEach(function(td) {
            const unm = td.textContent.trim();
            if (unm !== 'User name') {
                unmArr.push(unm);
            }
        });
        return unmArr;
    }

    function getunmArr() {
        const urls = ["https://karagarga.in/users.php?search=&class=40", "https://karagarga.in/users.php?search=&class=38"];
        let resps = 0;
        let mergednms = [];

        urls.forEach(function(url) {
            fetchmods(url, function(resptxt) {
                resps++;
                if (resptxt !== null) {
                    const usernames = extractnm(resptxt);
                    mergednms = mergednms.concat(usernames);
                }
                if (resps === urls.length) {
                    mods = mergednms.sort();
                    if (mods === null) {
                        mods = ["Aw", "corvusalbus", "flipflink", "hjulien44", "mproust87", "ronnie", "SalineLibations", "sjostrom27", "zappo"];
                    }
                    // console.log(mods);
                }
            });
        });
    }


    getunmArr();

    const sendMsgBtn = document.createElement("button");
    sendMsgBtn.textContent = "Upgrade Request";
    sendMsgBtn.style.cssText = "position: relative; font-weight: bold; color: #ffffff; background-color: #445672; border: none; border-radius: 3px; padding: 10px 18px; font-size: 14px; font-family: sans-serif; display: flex; align-items: center; justify-content: center; cursor: pointer; box-shadow: 0 2px 6px rgba(0, 0, 0, 0.2), 0 1px 4px rgba(0, 0, 0, 0.1); transition: transform 0.2s ease; margin-right: 0; margin-right: 0px; margin-left: auto";

    sendMsgBtn.addEventListener("click", function(event) {
        event.preventDefault();
        upgradeReqBox();
        addMediaInfoButton();
        sendMsgBtn.style.transform = "scale(0.95)";
        setTimeout(function() {
            sendMsgBtn.style.transform = "scale(1)";
        }, 200);
    });

    let ddimg = document.querySelector('div#ddimagetabs');
    let pelement = ddimg.parentElement;


    if (isTrumpable()) {
        pelement.insertBefore(sendMsgBtn, ddimg);
    } else {
        // Uncomment below line if you need on all torrent pages
        // pelement.insertBefore(sendMsgBtn, ddimg);
    }
})();
